# Use a imagem oficial do Python como base
FROM python:3.8-slim-buster

# Define o diretório de trabalho dentro do contêiner
WORKDIR /my-flask-app-docker

# Copie o código da aplicação para o contêiner
COPY requirements.txt requirements.txt

# Instale as dependências (neste caso, apenas o Flask)
RUN pip3 install -r requirements.txt 

# Exponha a porta em que a aplicação Flask será executada
COPY . .

# Comando para iniciar a aplicação Flask
CMD ["python3", "-m" , "flask", "run", "--host=0.0.0.0"]
