# DevOps AC5

## Projeto AWS ECS com Integração Contínua, Testes e Segurança

### Visão Geral
Este projeto é gerenciado no Jira, com atividades divididas para melhor acompanhamento. A documentação detalhada pode ser encontrada no arquivo README.md. Além disso, o projeto inclui os seguintes itens:

- **requirements.txt:** Arquivo contendo as dependências do projeto.
- **.gitignore:** Configuração para ignorar arquivos indesejados no repositório Git.
- **Docker:** A aplicação está preparada para execução em um contêiner Docker.
- **Segurança com Semgrep:** Validado o código em busca de possíveis vulnerabilidades.
- **Testes Unitários com PyUnit:** Testes unitários foram implementados para garantir a qualidade do código.
- **.gitlab-ci.yml:** Arquivo de configuração da pipeline para integração contínua no GitLab.
- **Pipeline no GitLab Runner:** A pipeline pode ser executada no GitLab Runner, garantindo automação eficiente.
- **Application Load Balancer:** A aplicação está acessível publicamente através de um Application Load Balancer.

### Estrutura do Projeto

- `app/`: Contém o código-fonte da aplicação.
- `scripts/`: Scripts de automação.
- `.github/workflows/`: Arquivos de configuração para CI/CD com GitHub Actions.

### Executando o Projeto
1. Clone este repositório.
2. Consulte o Jira para obter a divisão detalhada das atividades.
3. Execute a configuração da infraestrutura com o Terraform.
4. Configure sua ferramenta de CI/CD para automatizar builds e implantações no ECS.
5. Realize os testes unitários com PyUnit.
6. Execute a pipeline no GitLab Runner.
7. Acesse a aplicação publicamente através do Application Load Balancer.


### URL de Acesso ao ECS

https://us-east-1.console.aws.amazon.com/ecs/v2/clusters?region=us-east-1
