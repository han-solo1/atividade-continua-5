import unittest
from app import app

class check(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()


    def testtemplate(self):
        response = self.app.get("/")
        self.assertEqual(response.status_code, 200) 

    def test404(self):
        response = self.app.get('/erro_404')
        self.assertEqual(response.status_code, 404)
        

    def test300(self):
        response = self.app.get('/erro_300')
        self.assertEqual(response.status_code, 300)
        

    def test400(self):
        response = self.app.get('/erro_400')
        self.assertEqual(response.status_code, 400)
        

if __name__ == '__main__':
    unittest.main()
