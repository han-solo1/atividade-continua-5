from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/erro_404')
def erro_404():
    return 'Página não encontrada', 404

@app.route('/erro_400')
def erro_400():
    return 'Dados inválidos', 400

@app.route('/erro_300')
def erro_200():
    return 'Multiplas rotas', 300

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")

